from Car import *

class EntranceRamp():
    def __init__(self, highway, car_num, mode, ncars, frac):
        self.highway = highway
        self.car_num = car_num
        self.current_car  = 1
        self.wait_time = 0
        self.mode = mode
        self.ncars = ncars
        self.frac = frac
       
    def backup(self):
        self.wait_time = 5
        return self.wait_time                
    
    def tempDelay(self):
        # this method will only be called if a car is going to be within 0.10 m
        # of the start of the highway. It is called to ensure that no car can 
        # enter the highway during this timestep because it would result in 
        # having 2 cars that are too close to oneanother
        
        if self.wait_time ==0:
            self.wait_time = 1

    def carsWaiting(self):
        return self.car_num

    def advanceTime(self):
        if self.wait_time == 0 and self.current_car <= self.car_num:
            new_car = Car(self.current_car, self.highway, self.mode, 
                          self.ncars, self.frac)
            self.current_car += 1
            self.highway.addCar(new_car)
        if self.wait_time > 0:
            self.wait_time -= 1
            if self.wait_time <= 0:
                return 0
            else:
                return self.wait_time
            

