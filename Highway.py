import numpy as np
import matplotlib.pyplot as plt
from MileMarker import *
from Car import *
from EntranceRamp import *

class Highway(object):
    
#numcars is the total number of cars on the highway
#ifdeer can potentially exist or not, so its arguments will be either True or False
#if frac = None, then it'll randomly generate the speed cars slow down by

    def __init__(self,numcars,ifdeer,frac,ncars,deerchance):
        self.numcars = numcars
        self.deerchance = deerchance
        self.ifdeer = ifdeer
        self.frac = frac
        self.ncars = ncars
        self.time = 0
        self.markers = []
        for i in range(101):
            self.markers.append(MileMarker(i, deerchance))
        self.car_on_road = []
        self.car_exit = []
        self.ramp = EntranceRamp(self,numcars,"fixed",ncars,frac)
        self.numdeer = 0
        self.cars_to_remove = 0
        self.deerlocation = []

    def getNumDeer(self):
        return self.numdeer

    def addCar(self,newcar):
        self.car_on_road.append(newcar)

    def getCarPositions(self):
        l = []
        for car in self.car_on_road:
            l.append(car.getPosition())
        return l

    def getCarNumbers(self):
        # returns the numbers of each car on the road
        l = []
        for car in self.car_on_road:
            l.append(car.getNumber())
        return l

    def addCarToRemove(self, carremove):
        # appends the car to be removed to the exited cars and it adds one to 
        # the number of cars that will be removed
        self.cars_to_remove +=1
        self.car_exit.append(carremove)

    def removeCars(self):
        #ncars_to_remove = self.cars_to_remove
        if self.cars_to_remove > 0:
            #self.car_exit += deepcopy(self.car_on_road[:self.cars_to_remove])
            self.car_on_road = self.car_on_road[self.cars_to_remove:]
            self.cars_to_remove = 0
    
    def numCarOnRoad(self):
        return len(self.car_on_road)
        
    def numCarExited(self):
        return len(self.car_exit)
    def numCarWaiting(self):
        return self.numcars - self.numCarOnRoad() - self.numCarExited()
    def delayEntrance(self):
        self.ramp.backup()
    def findCar(self,car_num):
        o = self.numCarExited()
        if o >= car_num:
            return "exited"
        l = np.asarray([c.getNumber() for c in self.car_on_road])
        where = np.where(l == car_num)[0]
        if len(where) == 1:
            return self.car_on_road[where[0]]
        else: 
            return "has not entered"

    def advanceTime(self):
        deer = []
        if self.ifdeer:
            for marker in self.markers:
                d = marker.deerOccurrence()
                if d:
                    deer.append(marker.getMiles())
            self.numdeer = len(deer)
            self.deerlocation = deer
        for car in self.car_on_road:
            car.advanceTime(deer)
        self.removeCars()
        self.ramp.advanceTime()

    def tempDelay(self):
        self.ramp.tempDelay()

    def getDeerLoc(self):
        return self.deerlocation
