import numpy as np
import matplotlib.pyplot as plt
from MileMarker import *
from Car import *
from EntranceRamp import *
from Highway import *
import matplotlib.cm as cm



vary_cars = np.loadtxt('varying_cars_to_react.txt')

num_cars = vary_cars[:,0]
avg_time = vary_cars[:,1]
med_to_exit = vary_cars[:,2]
error = vary_cars[:,3]

vary_speed = np.loadtxt('varying_factor_by_which_car_speed_changes.txt')

factor_speed = vary_speed[:,0]
avg_time2 = vary_speed[:,1]
med_to_exit2 = vary_speed[:,2]
error2 = vary_speed[:,3]

vary_deerprob = np.loadtxt('varying_prob_of_deer.txt')
deer_prob = vary_deerprob[:,0]
avg_time3 = vary_deerprob[:,1]
med_to_exit3 = vary_deerprob[:,2]
error3 = vary_deerprob[:,3]



position = []
carnum = []
'''
def run_without_deer():
    h = Highway(1000,False,.5, 10, .20)
    n = h.numCarExited
    t = 0
    while n() != 1000 and t < 1102:
        h.advanceTime()
        t+=1
        #print 'Time = %d' % t
        
        #print h.getCarPositions()
        position.append(h.getCarPositions())
        carnum.append(h.getCarNumbers())
        #print h.numCarExited()
        #print h.numCarOnRoad()
        #print h.numCarWaiting()

run_without_deer()

'''

deer = []
def run_with_deer(frac = .5, prob=.10, ncars=10):
    h = Highway(1000,True, frac, ncars, prob)
    n = h.numCarExited
    t = 0
    while n() != 1000:
        h.advanceTime()
        t+=1
        position.append(h.getCarPositions())
        carnum.append(h.getCarNumbers())
        deer.append(h.getNumDeer())
    #return t

run_with_deer()



use_color = {'0':'white','1':'red', '2':'orange', '3':'yellow', '4':'green', '5':'blue', '6':'purple', '7':'grey', '8':'pink', '9':'black'}
 


def mod10(x):
    return x % 10

    
for i in range(len(position)):
    yaxis=np.zeros_like(position[i])
    colors2 = (mod10(x) for x in carnum[i])
    
    plt.scatter(position[i],yaxis,c=[use_color[str(x)[0]] for x in colors2])    
    plt.xlim(0,100)
    plt.ylim(-1,1)
    plt.title("Position of the Cars with Deer at Time " + str(i))    
    #plt.savefig('DeerPos/DeerTime' + str(i) + '.png')   
    plt.show()
    plt.close()


