import numpy as np

class Car:

    # the only methods ever used by other objects and by functions are:
    #  __init__(car_num, road)  -  when the object is defined 
    #  getNumber()
    #  getPosition()
    #  advanceTime()

    # ncars is the number of cars behind a car that sees a deer that overbreak
    # make sure that mode is 'fixed'
    # frac is the value by which the speed is multiplied by when a car 
    # overbreaks

    def __init__(self, car_num, road = None, mode = 'fixed', ncars = 10, 
                 frac = 0.7):
        # Check to see if car_num is an integer value. 
        #if not (isinstance(car_num, (int,long)) and car_num>= 1 and car_num <= 1000):
        #    raise RuntimeError('The variable car_num must be an integer value.')
        # Check to see that mode is eqaul to fixed
        #if not mode == 'fixed':
        #    raise RuntimeError('The parameter mode must be equal to fixed')
        # Check to see if ncars is a positive integer value
        #if not (isinstance(ncars, (int,long)) and ncars > 0):
        #    raise RuntimeError('The variable ncars must be a positive integer value.')
        # Check to see if frac is a floating point in the range (0,1]
        #if not (isinstance(frac, float) and frac >= 0 and frac <= 1.0):
        #    raise RuntimeError('The variable ncars must be a positive integer value.')

        self.car_num = car_num

        # mode expects either 'default' or 'fixed'
        # in 'default' mode, when people overbreak, all new velocities are 
        # randomly generated ensuring that the next car must be going just as 
        # slow if not slower as the car in front of it
        # in 'fixed' mode, when people overbreak they will slow down to the same
        # speed as the person who sees the deer
        self.mode = mode

        # ncars is the number of cars after the car that sees the deer that will
        # overbreak
        # this value must be an integer value above 0.
        self.ncars = ncars

        # frac is the fraction of the velocity that all cars that see a deer 
        # slow down by. If frac is None, then the velocity is randomly 
        # generated.
        # frac expects either None or a floating point number between 0 and 1
        self.frac = frac

        # road must be the Highwayobject
        if road is not None:
            self.road = road

        self.position = 0.0
        self.deer = False
        self.overbreak = False
        self.velocity = 1.0
        # this part might change. If the number of the first car is 1 then
        # the next part is correct. If the number of the first car is 0, then 
        # this function
        if car_num == 1:
            self.car_in_front = None
        else:
            a = road.findCar(car_num - 1)
            if isinstance(a, Car):
                self.car_in_front = a
                self.car_in_front.add_car_in_back(self)

        self.car_in_back = None

        #self.checkStatus()

    def getNumber(self):
        # returns the car number
        return self.car_num

    def getPosition(self):
        # This method returns the car's position
        return self.position

    def seenDeer(self):
        #self.checkStatus()
        # This method is called if a deer is seen by the car.
        self.deer = True
        if self.car_in_back is not None:
            self.car_in_back.overbreaking(self.ncars)

    def exitHighway(self):
        #assert(self.velocity > 0.0)
        #assert(self.position <= 101.0)
        #assert(self.position >= 100.0)
        #if self.car_in_back is not None:
        #    assert(self.car_in_back.position >= 0.0)
        #    assert(self.car_in_back.velocity > 0.0)
        #    assert((self.position - 0.10) >= self.car_in_back.position)
        #    assert(self.car_num == (self.car_in_back.car_num - 1))
        #else: 
        #    assert (not isinstance(self.road.findCar(self.car_num + 1), Car))

        if self.car_in_back is not None:
            self.car_in_back.remove_car_in_front()
        self.road.addCarToRemove(self)

    def overbreaking(self, num):
        if not (isinstance(num, (int,long)) and num >0):
            raise RuntimeError('The variable num is wrong')
        self.overbreak = True
        if (self.car_in_back is not None) and (num> 1):
            self.car_in_back.overbreaking(num-1)
    
    def remove_car_in_front(self):
        self.car_in_front = None

    def add_car_in_back(self,new_car):
        self.car_in_back = new_car

    def react(self):
        #self.checkStatus()
        # there are 2 modes: 'fixed' and 'default'
        if self.car_in_front is not None:
            v_car_in_front = self.car_in_front.velocity
        else:
            v_car_in_front = 1.0

        if self.position == 0.0 and (self.overbreak or self.deer):
            self.road.delayEntrance()

        if self.overbreak and self.deer:
            new_velocity = 1.0 * (self.frac**2.0)
            self.overbreak = False
            self.deer = False
        elif self.overbreak:
            new_velocity = 1.0 * (self.frac)
            self.overbreak = False
        elif self.deer:
            new_velocity = 1.0 * (self.frac)
            self.deer = False
        else:
            new_velocity = 1.0
        
        new_position = self.position + new_velocity

        if self.car_in_front is not None:
            # now we must ensure that the car maintains the a minimum distance 
            # between itself and the car in front.
            
            # Derrick suggested that we never allow two cars to get to within
            # 0.1 meters of one another
            
            # This is only ever going to be a problem if the car in front has a 
            # smaller velocity

            if (v_car_in_front < new_velocity):
                
                # we load the final position of the car in front
                
                

                p_in_front = self.car_in_front.position
                if (p_in_front - 0.10) <= (new_position):
                    # The car's new final position is going to be
                    # p_in_front - 0.10
                    # change in position during the time step will be:
                    # (p_in_front-0.1) - self.position
                    # velocity to achieve that change in position will be
                    # ((p_in_front-0.1) - self.position)/1 second
                    new_position = (p_in_front - 0.100001)
                    if not(p_in_front - new_position >= 0.1000):
                        new_position = (p_in_front - 0.10001)
                    new_velocity = new_position - self.position
                    #assert new_position <= (p_in_front - 0.1000), str(new_position)
                    #assert new_velocity >= 0.0, 'New velocity = %s. Change in position = %s' %(new_velocity, new_position)

        if self.position == 0.0 and new_velocity < 1.0:
            self.road.delayEntrance()

        self.position = new_position
        self.velocity = new_velocity
        #self.checkStatus()

    def determineMilemarker(self):
        smaller_mile_marker = float(int(self.position))
        if smaller_mile_marker == self.position:
            return int(self.position)
        else:
            return int(self.position) + 1

    def advanceTime(self, deer_occurance):
        if not isinstance(deer_occurance, list):
            raise RuntimeError('The variable deer_occurance must be a list.')
        #self.checkStatus()
        mile = self.determineMilemarker()
        # check for deer
        if mile in deer_occurance:
            self.seenDeer()
        # react to what's going on
        self.react()
        # if the velocity has slown down at all, and the car is at position 0
        # then we need to tell the highway to wait 5 seconds before it lets 
        # the next car onto it
        if self.position == 0.0 and self.velocity < 1.0:
            self.road.delayEntrance()
        elif (self.position) <= 0.10:
            self.road.tempDelay()
        if self.position >= 100.0:
            self.exitHighway()
        #self.checkStatus()
    """
    def checkStatus(self):
        # this method is designed to check the status of the car
        # if there is a car in front
        assert self.velocity >= 0.0, self.getInfo()
        assert(self.position >= 0.0)
        assert(self.position <= 101.0)
        if self.car_in_back is not None:
            assert(self.car_in_back.position >= 0.0)
            assert(self.car_in_back.velocity >= 0.0)
            assert((self.position - self.car_in_back.position) >= 0.0999)
            assert(self.car_num == (self.car_in_back.car_num - 1))
        if self.car_in_front is not None:
            assert self.car_in_front.position >= 0.10, self.getInfo() 
            assert(self.car_in_front.position <= 101.0)
            assert(self.car_in_front.velocity >= 0.0)
            assert (self.car_in_front.position - self.position) >= .0999, self.getInfo()
            assert(self.car_num == (self.car_in_front.car_num + 1))

    def getInfo(self):
        

        return "Car %s is at %s moving at %s m/s. Car %s is at %s moving at %s m/s." % (self.car_in_front.car_num, self.car_in_front.position, self.car_in_front.velocity, self.car_num, self.position, self.velocity)
    """
